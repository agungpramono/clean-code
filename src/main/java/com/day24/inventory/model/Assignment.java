package com.day24.inventory.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class Assignment {
    private int staff_id, item_id;
    private String created_at, returned_at;
}
