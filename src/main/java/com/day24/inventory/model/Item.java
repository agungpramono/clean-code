package com.day24.inventory.model;

import lombok.Data;

@Data
public class Item {
    private int id;
    private String name;
    private boolean status;
}
