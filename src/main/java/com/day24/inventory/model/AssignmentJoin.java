package com.day24.inventory.model;

import lombok.Data;

@Data
public class AssignmentJoin {
    private int id;
    private String staff_name, item_name, created_at, returned_at;
}
