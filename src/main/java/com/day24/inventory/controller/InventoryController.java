package com.day24.inventory.controller;

import com.day24.inventory.model.Assignment;
import com.day24.inventory.model.AssignmentJoin;
import com.day24.inventory.model.Item;
import com.day24.staff.model.Staff;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/inventory")
public class InventoryController {

    SqlSession session;

    public InventoryController() throws IOException {
        Reader reader = Resources.getResourceAsReader("SqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
        session = sqlSessionFactory.openSession();
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllInventory() throws Exception {
        refresh();
        JSONObject jsonObject = new JSONObject();
        List<Item> items = session.selectList("getAllInventory");
        jsonObject.put("status", 200);
        jsonObject.put("items", items);
        return new ResponseEntity<>(jsonObject, HttpStatus.OK);
    }

    @PostMapping("/insert")
    public ResponseEntity<?> insert(@RequestBody Item item) {
        item.setStatus(false);
        session.insert("insertInventory", item);
        session.commit(true);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status", 200);
        jsonObject.put("item", item);
        return new ResponseEntity<>(jsonObject, HttpStatus.OK);
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<?> getAllInventory(@PathVariable int id) throws Exception {
        refresh();
        JSONObject jsonObject = new JSONObject();
        Item item = session.selectOne("getInventoryById", id);
        if (item != null) {
            jsonObject.put("status", 200);
            jsonObject.put("item", item);
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        } else {
            jsonObject.put("status", 404);
            jsonObject.put("item", "Not found");
            return new ResponseEntity<>(jsonObject, HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateInventoryById(@PathVariable int id, @RequestBody Item item) throws Exception {
        refresh();
        JSONObject jsonObject = new JSONObject();
        Item target = session.selectOne("getInventoryById", id);
        if (target != null) {
            item.setId(id);
            session.update("updateInventory", item);
            session.commit(true);
            jsonObject.put("status", 200);
            jsonObject.put("item", item);
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        } else {
            jsonObject.put("status", 400);
            jsonObject.put("item", "Not found");
            return new ResponseEntity<>(jsonObject, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable int id) throws Exception {
        refresh();
        Item selectedItem = session.selectOne("getInventoryById", id);
        JSONObject jsonObject = new JSONObject();
        if (selectedItem == null) {
            jsonObject.put("status", 404);
            jsonObject.put("selectedItem", "Not found");
            return new ResponseEntity<>(jsonObject, HttpStatus.NOT_FOUND);
        } else {
            session.delete("deleteInventoryById", id);
            session.commit(true);
            jsonObject.put("status", 200);
            jsonObject.put("selectedItem", selectedItem);
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        }
    }

    @PostMapping("/assignItem")
    public ResponseEntity<?> assign(@RequestBody Assignment assignment) throws Exception {
        refresh();
        JSONObject jsonObject = new JSONObject();
        assignment.setCreated_at(getTimeNow());
        Item selectedItem = session.selectOne("getInventoryById", assignment.getItem_id());
        Staff selectedStaff = session.selectOne("getStaffById", assignment.getStaff_id());
        if (selectedItem == null) {
            jsonObject.put("status", 400);
            jsonObject.put("item", "Not found");
            return new ResponseEntity<>(jsonObject, HttpStatus.NOT_FOUND);
        } else if (selectedStaff == null) {
            jsonObject.put("status", 404);
            jsonObject.put("staff", "Not found");
            return new ResponseEntity<>(jsonObject, HttpStatus.NOT_FOUND);
        } else if (selectedItem == null && selectedStaff == null) {
            jsonObject.put("status", 404);
            jsonObject.put("item", "Not found");
            jsonObject.put("staff", "Not found");
            return new ResponseEntity<>(jsonObject, HttpStatus.NOT_FOUND);
        } else if (selectedItem != null && selectedStaff != null && selectedItem.isStatus()) {
            jsonObject.put("status", 404);
            jsonObject.put("item", "is used");
            return new ResponseEntity<>(jsonObject, HttpStatus.NOT_FOUND);
        } else {
            session.insert("insertAssignment", assignment);
            session.commit(true);
            selectedItem.setStatus(true);
            session.insert("updateInventory", selectedItem);
            session.commit(true);
            jsonObject.put("status", 200);
            jsonObject.put("item", assignment);
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        }
    }

    @GetMapping("/report/items")
    public ResponseEntity<?> getByItems() throws Exception {
        refresh();
        JSONObject jsonObject = new JSONObject();
        List<Item> itemList = session.selectList("getAllInventory");
        JSONArray jsonArray = new JSONArray();
        for (Item item : itemList) {
            JSONObject jsonItem = new JSONObject();
            jsonItem.put("item", item.getName());
            AssignmentJoin assignment = session.selectOne("getAssignmentByItem", item.getId());
            if (assignment != null) {
                jsonItem.put("detail", assignment);
                jsonArray.add(jsonItem);
            } else {
                jsonItem.put("detail", "is yet assigned");
                jsonArray.add(jsonItem);
            }
        }
        jsonObject.put("reports", jsonArray);
        return new ResponseEntity<>(jsonObject, HttpStatus.NOT_FOUND);
    }

    @GetMapping("/report/staffs")
    public ResponseEntity<?> getByStaff() throws Exception {
        refresh();
        JSONObject jsonObject = new JSONObject();
        List<Staff> staffList = session.selectList("getAllStaff");
        JSONArray jsonArray = new JSONArray();
        for (Staff staff : staffList) {
            JSONObject jsonStaff = new JSONObject();
            jsonStaff.put("staff", staff.getName());
            List<AssignmentJoin> assignment = session.selectList("getAssignmentByStaff", staff.getId());
            if (assignment != null) {
                jsonStaff.put("detail", assignment);
                jsonArray.add(jsonStaff);
            } else {
                jsonStaff.put("details", "is yet assigned");
                jsonArray.add(jsonStaff);
            }
        }
        jsonObject.put("reports", jsonArray);
        return new ResponseEntity<>(jsonObject, HttpStatus.OK);
    }

    private String getTimeNow() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return sdf.format(cal.getTime());
    }

    @GetMapping("/readById/{id}")
    public Item getItem(@PathVariable int id) throws Exception {
        refresh();
        Item item = session.selectOne("getInventoryById", id);
        if (item == null) {
            return null;
        } else {
            return item;
        }
    }

    @GetMapping("/return/item/{item_id}/staff/{staff_id}")
    public ResponseEntity<?> returnItem(@PathVariable int item_id, @PathVariable int staff_id) throws Exception {
        refresh();
        JSONObject jsonObject = new JSONObject();
        Item item = session.selectOne("getInventoryById", item_id);
        Staff staff = session.selectOne("getStaffById", staff_id);
        if (staff != null && item != null) {
            Map<String, Integer> payload = new HashMap<>();
            payload.put("item_id", item_id);
            payload.put("staff_id", staff_id);
            session.delete("deleteById", payload);
            session.commit(true);
            item.setStatus(false);
            session.update("updateInventory", item);
            session.commit(true);
        }
        jsonObject.put("status", 200);
        return new ResponseEntity<>(jsonObject, HttpStatus.OK);
    }

    public void refresh() throws Exception {
        session.close();
        Reader reader = Resources.getResourceAsReader("SqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
        session = sqlSessionFactory.openSession();
    }
}
