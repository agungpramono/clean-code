package com.day24.staff.model;

import lombok.Data;

@Data
public class Item {
    private int id;
    private String name;
    private boolean status;
}
