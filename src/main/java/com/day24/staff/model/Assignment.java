package com.day24.staff.model;

import lombok.Data;

@Data
public class Assignment {
    private int staff_id, item_id;
}
