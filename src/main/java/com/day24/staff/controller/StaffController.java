package com.day24.staff.controller;

import com.day24.staff.model.Assignment;
import com.day24.staff.model.AssignmentJoin;
import com.day24.staff.model.Item;
import com.day24.staff.model.Staff;
import com.day24.staff.service.AsyncService;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/staff")
public class StaffController {

    SqlSession session;
    @Autowired
    private static AsyncService service;

    public StaffController() throws IOException {
        Reader reader = Resources.getResourceAsReader("SqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
        session = sqlSessionFactory.openSession();
        service = new AsyncService();
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllStaff() throws Exception {
        refresh();
        JSONObject jsonObject = new JSONObject();
        List<Staff> staffs = session.selectList("getAllStaff");
        jsonObject.put("status", 200);
        jsonObject.put("staffs", staffs);
        return new ResponseEntity<>(jsonObject, HttpStatus.OK);
    }

    @PostMapping("/insert")
    public ResponseEntity<?> addStaff(@RequestBody Staff staff) {
        JSONObject jsonObject = new JSONObject();
        session.insert("insertStaff", staff);
        session.commit(true);
        jsonObject.put("status", 200);
        jsonObject.put("staff", staff);
        return new ResponseEntity<>(jsonObject, HttpStatus.OK);
    }

    @GetMapping("/findById/{id}")
    public ResponseEntity<?> getAllStaff(@PathVariable int id) throws Exception {
        refresh();
        JSONObject jsonObject = new JSONObject();
        Staff staff = session.selectOne("getStaffById", id);
        if (staff != null) {
            jsonObject.put("status", 200);
            jsonObject.put("staff", staff);
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        } else {
            jsonObject.put("status", 400);
            jsonObject.put("staff", "Not found");
            return new ResponseEntity<>(jsonObject, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/update/{id}")
    public ResponseEntity<?> updateStaffById(@PathVariable int id, @RequestBody Staff staff) {
        JSONObject jsonObject = new JSONObject();
        Staff target = session.selectOne("getStaffById", id);
        if (target != null) {
            staff.setId(id);
            session.update("updateStaff", staff);
            session.commit(true);
            jsonObject.put("status", 200);
            jsonObject.put("staff", staff);
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        } else {
            jsonObject.put("status", 400);
            jsonObject.put("staff", "Not found");
            return new ResponseEntity<>(jsonObject, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable int id) {
        Staff selectedStaff = session.selectOne("getStaffById", id);
        JSONObject jsonObject = new JSONObject();
        if (selectedStaff == null) {
            jsonObject.put("status", 404);
            jsonObject.put("staff", "Not found");
            return new ResponseEntity<>(jsonObject, HttpStatus.NOT_FOUND);
        } else {
            session.delete("deleteStaffById", id);
            session.commit(true);
            jsonObject.put("status", 200);
            jsonObject.put("staff", selectedStaff);
            return new ResponseEntity<>(jsonObject, HttpStatus.OK);
        }
    }

    @PostMapping("/assignItem")
    public ResponseEntity<?> assign(@RequestBody Assignment assignment) throws Exception {
        JSONObject jsonObject = new JSONObject();
        CompletableFuture<Item> itemCompletableFuture = service.getItem(assignment.getItem_id());
        ResponseEntity<?> staff = getAllStaff(assignment.getStaff_id());
        CompletableFuture.allOf(itemCompletableFuture).join();
        if (itemCompletableFuture.isDone()) {
            if (staff.getStatusCodeValue() == 404 && itemCompletableFuture.get() == null) {
                jsonObject.put("status", 404);
                jsonObject.put("message", "staff dan item tidak ditemukan");
            } else if (staff.getStatusCodeValue() == 200 && itemCompletableFuture.get() == null) {
                jsonObject.put("status", 404);
                jsonObject.put("message", "Item tidak ditemukan");
            } else if (staff.getStatusCodeValue() == 404 && itemCompletableFuture.get() != null) {
                jsonObject.put("status", 404);
                jsonObject.put("message", "Staff tidak ditemukan");
            } else {
                CompletableFuture assign = service.sendRequest(assignment);
                jsonObject.put("status", 200);
                jsonObject.put("message", assign);
            }
        }
        return new ResponseEntity<>(jsonObject, HttpStatus.OK);
    }

    @GetMapping("/report/items")
    public ResponseEntity<?> getByItems() throws Exception {
        refresh();
        JSONObject jsonObject = new JSONObject();
        List<Item> itemList = session.selectList("getAllInventoryStaff");
        JSONArray jsonArray = new JSONArray();
        for (Item item : itemList) {
            JSONObject jsonItem = new JSONObject();
            jsonItem.put("item", item.getName());
            AssignmentJoin assignment = session.selectOne("getAssignmentByItem2", item.getId());
            if (assignment != null) {
                jsonItem.put("detail", assignment);
                jsonArray.add(jsonItem);
            } else {
                jsonItem.put("detail", "is yet assigned");
                jsonArray.add(jsonItem);
            }
        }
        jsonObject.put("reports", jsonArray);
        return new ResponseEntity<>(jsonObject, HttpStatus.NOT_FOUND);
    }

    @GetMapping("/report/staffs")
    public ResponseEntity<?> getByStaff() throws Exception {
        refresh();
        JSONObject jsonObject = new JSONObject();
        List<Staff> staffList = session.selectList("getAllStaff");
        JSONArray jsonArray = new JSONArray();
        for (Staff staff : staffList) {
            JSONObject jsonStaff = new JSONObject();
            jsonStaff.put("staff", staff.getName());
            List<AssignmentJoin> assignment = session.selectList("getAssignmentByStaff2", staff.getId());
            if (assignment != null) {
                jsonStaff.put("detail", assignment);
                jsonArray.add(jsonStaff);
            } else {
                jsonStaff.put("details", "is yet assigned");
                jsonArray.add(jsonStaff);
            }
        }
        jsonObject.put("reports", jsonArray);
        return new ResponseEntity<>(jsonObject, HttpStatus.OK);
    }

    @GetMapping("/return/item/{item_id}/staff/{staff_id}")
    public ResponseEntity<?> returnItem(@PathVariable int item_id, @PathVariable int staff_id) throws Exception {
        refresh();
        JSONObject jsonObject = new JSONObject();
        CompletableFuture<JSONObject> itemCompletableFuture = service.returnItem(item_id, staff_id);
        CompletableFuture.allOf(itemCompletableFuture).join();
        if (itemCompletableFuture.isDone()) {
            jsonObject.put("status", 404);
        }
        return new ResponseEntity<>(jsonObject, HttpStatus.OK);
    }

    public void refresh() throws Exception {
        session.close();
        Reader reader = Resources.getResourceAsReader("SqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
        session = sqlSessionFactory.openSession();
        service = new AsyncService();
    }
}
