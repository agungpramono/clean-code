package com.day24.staff.service;

import com.day24.staff.model.Assignment;
import com.day24.staff.model.Item;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Service
public class AsyncService {
    @Autowired
    private RestTemplate restTemplate;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public AsyncService() {
        restTemplate = new RestTemplate();
    }

    @Async("asyncExecutor")
    public CompletableFuture<Item> getItem(int id) {
        Item item = restTemplate.getForObject("http://localhost:8080/api/inventory/readById/" + id, Item.class);
        if (item != null) {
            return CompletableFuture.completedFuture(item);
        } else {
            return CompletableFuture.completedFuture(null);
        }
    }

    @Async("asyncExecutor")
    public CompletableFuture<?> sendRequest(Assignment assignmentReq) {
        Assignment assignment = restTemplate.postForObject("http://localhost:8080/api/inventory/assignItem",
                assignmentReq, Assignment.class);
        if (assignment != null) {
            return CompletableFuture.completedFuture(assignment);
        } else {
            return CompletableFuture.completedFuture(null);
        }
    }

    @Async("asyncExecutor")
    public CompletableFuture<JSONObject> returnItem(int item_id, int staff_id) {
        JSONObject assignment = restTemplate.getForObject("http://localhost:8080/api/inventory/return/item/"
                + item_id + "/staff/" + staff_id, JSONObject.class);
        if (assignment != null) {
            return CompletableFuture.completedFuture(assignment);
        } else {
            return CompletableFuture.completedFuture(null);
        }
    }
}
